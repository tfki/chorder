#include <array>
#include <chrono>
#include <cmath>
#include <complex>
#include <iostream>
#include <pulse/simple.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <thread>
#include <algorithm>
#include <ranges>

#include <chorder/notes.hpp>
#include <chorder/literals.hpp>
#include <chorder/guitar/chord.hpp>
#include <chorder/fourier.hpp>
#include <chorder/guitar/tuning.hpp>
#include <chorder/ncurses/main_menu.hpp>

#include <curses.h>
#include <panel.h>
#include <menu.h>

const float pi = 3.14159265359;

float hann_window(int length, int x) {
	return static_cast<float>(std::pow(
			std::sin(pi * static_cast<float>(x) / static_cast<float>(length)), 2));
}

int main() {
	initscr();

	chorder::ncurses::MainMenu main_menu{};
	main_menu.MainLoop();

	return 0;
}

int main2() {
	using namespace chorder::literals;
	const std::vector<float> frequencies = chorder::Note::GetFrequenciesInRange(2_E, 6_D);

	pa_simple *s;
	pa_sample_spec ss;
	pa_buffer_attr ba;

	ba.fragsize = 100;

	ss.format = PA_SAMPLE_FLOAT32NE;
	ss.channels = 1;
	ss.rate = 2400;

	s = pa_simple_new(nullptr,                     // Use the default server.
					  "ncurses_spectrum_analyzer", // Our application's name.
					  PA_STREAM_RECORD,
					  nullptr,                     // Use the default device.
					  "ncurses_spectrum_analyzer", // Description of our stream.
					  &ss,                         // Our sample format.
					  nullptr,                     // Use default channel map
					  &ba,    // Use default buffering attributes.
					  nullptr // Ignore error code.
	);

	const int PA_BUFFER_SIZE = 512;

	std::array<float, PA_BUFFER_SIZE> buffer{};

//	initscr();
//	clear();
//	noecho();
//	cbreak();
//	keypad(stdscr, true);
//	timeout(50);
//	notimeout(stdscr, false);

	int hz_offset = 0;
	while (1) {
		pa_simple_read(s, buffer.data(), buffer.size() * sizeof(float), nullptr);

		auto t1 = std::chrono::high_resolution_clock::now();

		std::vector<float> ft_in{};
		ft_in.reserve(PA_BUFFER_SIZE);
		for (int i = 0; i < PA_BUFFER_SIZE; i++) {
			ft_in.emplace_back(buffer[i] * hann_window(PA_BUFFER_SIZE, i));
		}

		auto ft_out = chorder::fft_cooley_tukey(static_cast<float>(ft_in.size()) / static_cast<float>(ss.rate), ft_in,
												frequencies);

		auto t2 = std::chrono::high_resolution_clock::now();

		auto diff = std::chrono::duration<double, std::milli>(t2 - t1);
		std::cout << diff.count() << "ms" << std::endl;

		int max_i = 0;
		for (int i = 0; i < ft_out.size(); i++) {
			if (std::abs(ft_out[i]) > std::abs(ft_out[max_i]))
				max_i = i;
		}
		std::cout << "max: " << frequencies[max_i] << std::endl << std::flush;

//		switch (getch()) {
//			case KEY_LEFT:
//				hz_offset -= 10;
//				break;
//			case KEY_RIGHT:
//				hz_offset += 10;
//				break;
//		}
//
//		clear();
//		double max_val = 10;
//		float step = static_cast<float>(max_val) / static_cast<float>(LINES);
//		for (int i = 0; i < LINES; i++) {
//			for (int j = 0; j < ft_out.size() && j < COLS; j++) {
//				double distance = std::abs(ft_out[j + hz_offset]);
////				double distance = std::abs(std::complex<float>{ft_out[j + hz_offset][0], ft_out[j + hz_offset][1]});
//
//				if (distance > static_cast<int>(static_cast<float>((LINES - i)) * step))
//					mvaddch(i, j, '|');
//				else
//					mvaddch(i, j, ' ');
//			}
//		}

//		mvprintw(0, 0, "%f ms", diff.count());
//
//		std::string label_left = std::to_string(hz_offset) + " Hz";
//		std::string label_right = std::to_string(hz_offset + COLS) + " Hz";
//		mvprintw(LINES - 1, 0, "%s", label_left.c_str());
//		mvprintw(LINES - 1, COLS - label_right.length(), "%s", label_right.c_str());
//
		refresh();
	}

	pa_simple_free(s);
}
