#ifndef CHORDER_TUNING_HPP
#define CHORDER_TUNING_HPP

#include <chorder/notes.hpp>

#include <cstddef>
#include <vector>

namespace chorder::guitar {
	class Tuning {
	public:
		Tuning(std::initializer_list<Note> notes) : notes_(notes) {}

		[[nodiscard]] std::vector<Note> Notes() const { return notes_; }

	private:
		std::vector<Note> notes_;
	};
}

#endif //CHORDER_TUNING_HPP
