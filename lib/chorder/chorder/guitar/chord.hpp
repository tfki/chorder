#ifndef CHORDER_CHORD_HPP
#define CHORDER_CHORD_HPP

#include <chorder/guitar/tuning.hpp>

#include <utility>
#include <vector>
#include <stdexcept>
#include <string>

namespace chorder::guitar {
	struct string_action {
		string_action(bool strum = false, int fret = 0) : strum(strum), fret(fret) {}

		bool strum;
		int fret;
	};

	class Chord {
	public:
		Chord(std::string name, Tuning tuning, std::initializer_list<string_action> string_actions) : tuning_(
				std::move(tuning)), name_(std::move(name)), string_actions_(string_actions) {

			if (tuning_.Notes().size() != string_actions_.size())
				throw std::runtime_error("chord must specify an action for each string in tuning");
		}

		[[nodiscard]] const std::vector<string_action> &Actions() const { return string_actions_; }

		[[nodiscard]] std::string Name() const { return name_; }

		[[nodiscard]] Tuning Tuning() const { return tuning_; }

	private:
		class Tuning tuning_;
		std::string name_;
		std::vector<string_action> string_actions_;
	};
}

#endif //CHORDER_CHORD_HPP
