#ifndef CHORDER_FOURIER_HPP
#define CHORDER_FOURIER_HPP

#include <vector>
#include <complex>

namespace chorder {
	std::vector<std::complex<float>>
	ft(float duration, const std::vector<float> &ft_in, const std::vector<float> &frequencies);

	std::vector<std::complex<float>>
	fft_cooley_tukey(float duration, const std::vector<float> &ft_in, const std::vector<float> &frequencies);
}

#endif //CHORDER_FOURIER_HPP
