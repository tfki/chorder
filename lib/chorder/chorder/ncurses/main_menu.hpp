#ifndef CHORDER_MAIN_MENU_HPP
#define CHORDER_MAIN_MENU_HPP

#include <string>
#include <vector>
#include <ncurses.h>

namespace chorder::ncurses {
	class MainMenu {
	public:
		MainMenu() {
			selected_item_ = 0;
			menu_items_ = {"Random Practice", "Deterministic Practice", "Edit Chords", "Show A chord", "Exit"};
		}

		[[noreturn]] void MainLoop();

	private:
		int selected_item_;
		std::vector<std::string> menu_items_;
	};
}

#endif //CHORDER_MAIN_MENU_HPP
