#ifndef CHORDER_SHOW_CHORD_HPP
#define CHORDER_SHOW_CHORD_HPP

#include "chorder/guitar/chord.hpp"

#include <ncurses.h>

namespace chorder::ncurses {
	class ShowChord {
	public:
		explicit ShowChord(guitar::Chord chord, int minheight = 10) : chord_(std::move(chord)), minheight_(minheight) {}

		void MainLoop();

	private:
		guitar::Chord chord_;
		int minheight_;
	};
}

#endif //CHORDER_SHOW_CHORD_HPP
