#ifndef CHORDER_NOTES_HPP
#define CHORDER_NOTES_HPP

#include <vector>
#include <string>
#include <stdexcept>
#include <cmath>

namespace chorder {
	const float A4_freq = 440;

	enum NoteLetter {
		C = 0, Csh = 1, D = 2, Dsh = 3, E = 4, F = 5, Fsh = 6, G = 7, Gsh = 8, A = 9, Ash = 10, B = 11
	};

	static std::string NoteLetterToString(NoteLetter letter) {
		switch (letter) {
			case C: return "C";
			case Csh: return "C#";
			case D: return "D";
			case Dsh: return "D#";
			case E: return "E";
			case F: return "F";
			case Fsh: return "F#";
			case G: return "G";
			case Gsh: return "G#";
			case A: return "A";
			case Ash: return "Ash";
			case B: return "B";
		}
		throw std::runtime_error("unknown note " + std::to_string(letter));
	}

	class Note {
	public:
		Note(NoteLetter note_letter, int octave) : note_letter_(note_letter), octave_(octave) {}

		[[nodiscard]] NoteLetter NoteLetter() const { return note_letter_; };

		[[nodiscard]] int Octave() const { return octave_; }

		[[nodiscard]] float Frequency() const;

		[[nodiscard]] bool operator==(const Note &other);

		[[nodiscard]] bool operator!=(const Note &other);

		Note &operator+=(int x);

		[[nodiscard]] static std::vector<float> GetFrequenciesInRange(const Note &start,const Note &end);
	private:
		enum NoteLetter note_letter_;
		int octave_;
	};
}

#endif //CHORDER_NOTES_HPP
