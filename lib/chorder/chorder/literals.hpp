
#ifndef CHORDER_LITERALS_HPP
#define CHORDER_LITERALS_HPP

#include <chorder/notes.hpp>

namespace chorder::literals {
	inline Note operator "" _A(unsigned long long octave) {
		return Note{A, static_cast<int>(octave)};
	}

	inline Note operator "" _Ash(unsigned long long octave) {
		return Note{Ash, static_cast<int>(octave)};
	}

	inline Note operator "" _B(unsigned long long octave) {
		return Note{B, static_cast<int>(octave)};
	}

	inline Note operator "" _C(unsigned long long octave) {
		return Note{C, static_cast<int>(octave)};
	}

	inline Note operator "" _Csh(unsigned long long octave) {
		return Note{Csh, static_cast<int>(octave)};
	}

	inline Note operator "" _D(unsigned long long octave) {
		return Note{D, static_cast<int>(octave)};
	}

	inline Note operator "" _Dsh(unsigned long long octave) {
		return Note{Dsh, static_cast<int>(octave)};
	}

	inline Note operator "" _E(unsigned long long octave) {
		return Note{E, static_cast<int>(octave)};
	}

	inline Note operator "" _F(unsigned long long octave) {
		return Note{F, static_cast<int>(octave)};
	}

	inline Note operator "" _Fsh(unsigned long long octave) {
		return Note{Fsh, static_cast<int>(octave)};
	}

	inline Note operator "" _G(unsigned long long octave) {
		return Note{G, static_cast<int>(octave)};
	}

	inline Note operator "" _Gsh(unsigned long long octave) {
		return Note{Gsh, static_cast<int>(octave)};
	}
}

#endif //CHORDER_LITERALS_HPP
