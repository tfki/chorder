#include <chorder/ncurses/main_menu.hpp>

#include <chorder/guitar/guitar.hpp>
#include <chorder/ncurses/show_chord.hpp>

void draw_logo() {
	std::string line1 = R"(           /$$                                 /$$                    )";
	std::string line2 = R"(          | $$                                | $$                    )";
	std::string line3 = R"(  /$$$$$$$| $$$$$$$   /$$$$$$   /$$$$$$   /$$$$$$$  /$$$$$$   /$$$$$$ )";
	std::string line4 = R"( /$$_____/| $$__  $$ /$$__  $$ /$$__  $$ /$$__  $$ /$$__  $$ /$$__  $$)";
	std::string line5 = R"(| $$      | $$  \ $$| $$  \ $$| $$  \__/| $$  | $$| $$$$$$$$| $$  \__/)";
	std::string line6 = R"(| $$      | $$  | $$| $$  | $$| $$      | $$  | $$| $$_____/| $$      )";
	std::string line7 = R"(|  $$$$$$$| $$  | $$|  $$$$$$/| $$      |  $$$$$$$|  $$$$$$$| $$      )";
	std::string line8 = R"( \_______/|__/  |__/ \______/ |__/       \_______/ \_______/|__/      )";

	int line_length = static_cast<int>(line1.size());
	int x_offset = (COLS - line_length) / 2;
	int y_offset = 5;

	mvprintw(0 + y_offset, x_offset, "%s", line1.c_str());
	mvprintw(1 + y_offset, x_offset, "%s", line2.c_str());
	mvprintw(2 + y_offset, x_offset, "%s", line3.c_str());
	mvprintw(3 + y_offset, x_offset, "%s", line4.c_str());
	mvprintw(4 + y_offset, x_offset, "%s", line5.c_str());
	mvprintw(5 + y_offset, x_offset, "%s", line6.c_str());
	mvprintw(6 + y_offset, x_offset, "%s", line7.c_str());
	mvprintw(7 + y_offset, x_offset, "%s", line8.c_str());
}

void draw_menu(int selected_i, const std::vector<std::string> &menu_items) {
	int longest_menu_item = 0;
	for (const auto &item: menu_items) {
		if (item.size() > longest_menu_item)
			longest_menu_item = static_cast<int>(item.size());
	}
	int x_offset = (COLS - longest_menu_item) / 2;
	int y_offset = 15;

	for (int i = 0; i < menu_items.size(); i++) {
		mvprintw(y_offset + 2 * i, x_offset, "%s", menu_items[i].c_str());
	}
	mvprintw(y_offset + 2 * selected_i, x_offset - 2, ">");
}

[[noreturn]] void chorder::ncurses::MainMenu::MainLoop() {
	clear();
	start_color();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);

	int i = 0;
	while (true) {
		if (i == KEY_DOWN)
			selected_item_ = (selected_item_ + 1) % static_cast<int>(menu_items_.size());
		if (i == KEY_UP) {
			if (selected_item_ - 1 < 0) selected_item_ = static_cast<int>(menu_items_.size()) - 1;
			else selected_item_--;
		}
		if (i == '\n' && menu_items_[selected_item_] == "Show A chord") {
			using namespace chorder::literals;
			chorder::guitar::Tuning tuning{2_E, 2_A, 3_D, 3_G, 3_B, 4_E};
			chorder::guitar::Chord a{"A", tuning, {{false}, {true, 0}, {true, 2}, {true, 2}, {true, 2}, {true}}};

			ncurses::ShowChord show_a{a};
			show_a.MainLoop();
		}
		if (i == '\n' && menu_items_[selected_item_] == "Exit") {
			endwin();
			exit(0);
		}


		clear();
		border(0, 0, 0, 0, 0, 0, 0, 0);
		draw_logo();
		draw_menu(selected_item_, menu_items_);
		refresh();
		i = getch();
	}
}
