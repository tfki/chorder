#include <chorder/ncurses/show_chord.hpp>

#include <chrono>

void draw_chord(const chorder::guitar::Chord &chord, int minheight) {
	int maxfret = std::numeric_limits<int>::min();
	for (auto action: chord.Actions()) {
		if (action.fret > maxfret)
			maxfret = action.fret;
	}

	const int height = std::max(maxfret * 2 + 2, minheight);
	const int width = static_cast<int>(chord.Actions().size()) * 2 - 1;

	int y_offset = (LINES - height) / 2;
	{
		int x_offset = (COLS - chord.Name().length()) / 2;
		mvprintw(y_offset, x_offset, "%s", chord.Name().c_str());
	}
	{
		int x_offset = (COLS - width) / 2;

		for (int y = 2; y < height; y++) {
			for (int x = 0; x < chord.Actions().size(); x++) {
				if (chord.Actions()[x].strum)
					attroff(A_DIM);
				else
					attron(A_DIM);

				if (chord.Actions()[x].fret == y - 1)
					mvprintw(y + y_offset, 2 * x + x_offset, "O ");
				else
					mvprintw(y + y_offset, 2 * x + x_offset, "| ");
			}
		}
	}
}

void chorder::ncurses::ShowChord::MainLoop() {
	clear();

	curs_set(0);

	do {
		clear();
		border(0, 0, 0, 0, 0, 0, 0, 0);
		draw_chord(chord_, minheight_);
		refresh();
	} while (getch() == KEY_RESIZE);
}
