#include <chorder/notes.hpp>

float chorder::Note::Frequency() const {
	int octave_diff = octave_ - 4;
	int letter_diff = note_letter_ - A;
	int half_steps_to_A4 = octave_diff * 12 + letter_diff;

	float freq = A4_freq * static_cast<float>(std::pow(2, half_steps_to_A4 / 12.0));

	return freq;
}

std::vector<float> chorder::Note::GetFrequenciesInRange(const chorder::Note &start, const chorder::Note &end) {
	std::vector<float> result{};

	Note i{start};
	while (i != end) {
		result.emplace_back(i.Frequency());
		i += 1;
	}
	result.emplace_back(i.Frequency());

	return result;
}

bool chorder::Note::operator==(const chorder::Note &other) {
	return other.note_letter_ == note_letter_ && other.octave_ == octave_;
}

chorder::Note &chorder::Note::operator+=(int x) {
	for (int i = 0; i < x; i++) {
		if (note_letter_ == B) {
			note_letter_ = C;
			octave_++;
		} else
			note_letter_ = static_cast<enum NoteLetter>(note_letter_ + 1);
	}
	return *this;
}

bool chorder::Note::operator!=(const chorder::Note &other) {
	return !operator==(other);
}
