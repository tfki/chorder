#include <chorder/fourier.hpp>

const float pi = 3.14159265359;

std::vector<std::complex<float>>
chorder::ft(float duration, const std::vector<float> &ft_in, const std::vector<float> &frequencies) {
	std::vector<std::complex<float>> result{};

	for (float freq: frequencies) {
		std::complex<float> sum{};

		for (int i = 0; i < ft_in.size(); i++) {
			float t = duration * static_cast<float>(i) / static_cast<float>(ft_in.size());
			float x = -2.0f * pi * t * freq;
			sum += ft_in[i] * std::complex<float>{std::cos(x), std::sin(x)};
		}

		sum /= duration;
		result.emplace_back(sum);
	}
	return result;
}

std::vector<std::complex<float>>
chorder::fft_cooley_tukey(float duration, const std::vector<float> &ft_in, const std::vector<float> &frequencies) {
	std::vector<std::complex<float>> result{};
	for (float freq: frequencies) {
		std::vector<std::complex<float>> factors{};

		int divide_steps = 0;
		int sums_count = 1;
		std::size_t sum_length = ft_in.size();
		{
			float tmp = duration * -2.0f * pi * freq;
			while (sum_length % 2 == 0 && divide_steps < 32 && sum_length / 2 > 16) {
				float x = tmp / static_cast<float>(sum_length);
				factors.emplace_back(std::cos(x), std::sin(x));

				sum_length /= 2;
				sums_count *= 2;
				divide_steps++;
			}
		}

		auto sums = std::vector<std::complex<float>>(sums_count);

		float tmp = -2.0f * pi * freq * duration / static_cast<float>(sum_length);
		for (std::size_t i = 0; i < ft_in.size(); i += sums_count) {
			float x = tmp * static_cast<float>(i / sums_count);
			std::complex<float> exp_part_of_ft{std::cos(x), std::sin(x)};

			for (std::size_t j = 0; j < sums_count; j++) {
				sums[j] += ft_in[i + j] * exp_part_of_ft;
			}
		}

		for (int sum_i = 0; sum_i < sums_count; sum_i++) {
			for (int factor_i = 0; factor_i < factors.size(); factor_i++) {
				if (sum_i & (1 << (factor_i))) {
					sums[sum_i] *= factors[factor_i];
				}
			}
		}

		std::complex<float> sum{};
		for (auto summand: sums) {
			sum += summand;
		}

		sum /= duration;
		result.emplace_back(sum);
	}
	return result;
}
